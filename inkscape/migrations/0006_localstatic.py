# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2021-03-04 02:11
from __future__ import unicode_literals

from django.db import migrations, models
import inkscape.utils


class Migration(migrations.Migration):

    dependencies = [
        ('inkscape', '0005_auto_20181016_1557'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalStatic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('static_file', models.FileField(storage=inkscape.utils.ReplaceStore(), upload_to='../static')),
                ('file_type', models.CharField(choices=[('css', 'Style Sheet'), ('js', 'Javascript'), ('img', 'Image'), ('?', 'Unknown')], max_length=3)),
            ],
        ),
    ]
