
from django.conf import settings
from sekizai.templatetags.sekizai_tags import *

class InkscapeRenderBlock(RenderBlock):
    def render_tag(self, context, name, postprocessor, nodelist):
        if settings.IS_TEST:
            return nodelist.render(context)
        return super().render_tag(context, name, postprocessor, nodelist)

register.tag('inkscape_render_block', InkscapeRenderBlock)
